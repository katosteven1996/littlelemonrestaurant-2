//
//  HomeView.swift
//  LittleLemonRestaurant
//
//  Created by Mwine on 13/03/2023.
//

import SwiftUI

var foodData = foodArray().foodData
struct HomeView: View {
    @State private var orderSearch = ""
    var body: some View {
        ScrollView{
            LittleLemonLogo()
//            VStack{
                Image("woman_profile")
                    .resizable()
                    .clipShape(Circle())
                    .aspectRatio( contentMode: .fit)
                    .frame( width:350,height: 50, alignment: .topTrailing)
                    .padding(.vertical,0)
                    .overlay(Circle().stroke(Color.white, lineWidth: 4))
                    .shadow(radius: 7)
                
                
                
                
                ZStack{
                    Rectangle()
                        .frame(width: 428, height: 350)
                        .foregroundColor(Color("myGreen"))
                    VStack{
                        VStack(alignment: .leading){
                            Text("Little lemon")
                                .font(.system(size: 40, weight: .bold, design: .serif))
                            //                            .frame(width: 428, alignment: .leading)
                            //                            .offset(x:50)
                            
                            
                            
                            Text("Chicago")
                                .font(.system(size: 35, weight: .bold, design: .serif))
                            //                            .frame(width: 428, alignment: .leading)
                            //                            .offset(x:50)
                        }.frame( width:350, alignment: .topLeading)
                        
                        HStack(spacing:30){
                            Text("We are a family owned Mediterranean restaurant, focused on traditional recipes served with a modern twist.")
                                .font(.system(size: 15))
                            Image("Greek salad")
                                .resizable()
                                .renderingMode(.original)
                                .aspectRatio(contentMode: .fit)
                                .frame(width:200)
                                .cornerRadius(5)
                            
                        }.padding(.horizontal,30)
//                        Form{
                        ZStack{
                            Rectangle()
                                .frame(width: 350, height: 50)
                                .foregroundColor(.white)
                                .cornerRadius(10)
                           
                                HStack(spacing: 10){
                                    Image(systemName: "magnifyingglass")

                                    TextField("Search", text: $orderSearch)
                            
                                }.frame(width: 300)
                            }
                            
//                        }
                    }
                    .frame(alignment:.topLeading)
                    
                }
                Text("ORDER FOR DELIVERY")
                    .bold()
                    .font(.title2)
                    .underline()
                    .frame(width: 350, alignment: .leading)
                
                HStack(spacing: 25){
                    ZStack{
                        Rectangle()
                            .frame(width:70, height: 35)
                            .foregroundColor(.gray)
                            .cornerRadius(5)
                        Text("Starters")
                            .font(.caption)
                            .bold()
                    }
                    ZStack{
                        Rectangle()
                            .frame(width:70, height: 35)
                            .foregroundColor(.gray)
                            .cornerRadius(5)
                        Text("Starters")
                            .font(.caption)
                            .bold()
                    }
                    ZStack{
                        Rectangle()
                            .frame(width:70, height: 35)
                            .foregroundColor(.gray)
                            .cornerRadius(5)
                        Text("Starters")
                            .font(.caption)
                            .bold()
                    }
                    ZStack{
                        Rectangle()
                            .frame(width:70, height: 35)
                            .foregroundColor(.gray)
                            .cornerRadius(5)
                        Text("Starters")
                            .font(.caption)
                            .bold()
                    }
                    
                }
                Text("fbajkbvj;kabkjasbdvjk;basd;kvbadksbdsajb;jbaj;bjd;a")
                    .foregroundColor(.white)
                    .frame(width:400)
                    .underline(pattern: .dot, color: .black)
            
                LazyVStack{
                    ForEach( foodData, id: \.id){datum in
                        HStack{
                            
                            VStack(alignment: .leading){
                                Text(datum.foodName)
                                    .font(.title2)
                                    .fontWeight(.medium)
                                    .frame(width: 230, alignment: .leading)
                                Text(datum.foodDescription)
                                    .lineLimit(2)
                                Text("$" + String(datum.foodPrice))
                                    .fontWeight(.medium)
                                    .frame(width: 230, alignment: .leading)
                            }
                            Image(datum.foodImage)
                                .resizable()
                                .aspectRatio(contentMode: .fit)
                                .frame(width: 100)
                        }
                        
                        
                    }
                }.frame(width: 360)
                                
            }
                
                
                

            
        
    }
}

struct HomeView_Previews: PreviewProvider {
    static var previews: some View {
        HomeView()
    }
}
