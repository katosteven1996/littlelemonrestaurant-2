//
//  Homedata.swift
//  LittleLemonRestaurant
//
//  Created by Mwine on 13/03/2023.
//

import Foundation
import SwiftUI



struct FoodDetails {
var foodName:String
    var foodDescription:String
    var foodPrice:Double
    var foodImage:String
    var id = UUID()
    
}

struct foodArray{
    
    
    let foodData = [
        FoodDetails(foodName: "Greek Salad", foodDescription: "The famous greek salad of crispy lettuce, peppers, olives and our Chicago style feta cheese, garnished with crunchy garlic and rosemary croutons. ", foodPrice: 12.99, foodImage: "Greek salad"),
        FoodDetails(foodName: "Brushetta", foodDescription: "Our Bruschetta is made from grilled bread that has been smeared with garlic and seasoned with salt and olive oil. Toppings of tomato, veggies, beans, cured pork, or cheese are examples of variations. In Italy, a brustolina grill is frequently used to create bruschetta.", foodPrice: 7.99, foodImage: "Brushetta"),
        FoodDetails(foodName: "Grilled Fish", foodDescription: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam sed cursus.", foodPrice: 20.0, foodImage: "Grilled fish")
    ]}


struct CustomColor {
    public static let myGreen  = Color("myGreen")
    public static let myYellow  = Color("myYellow")
    
}
