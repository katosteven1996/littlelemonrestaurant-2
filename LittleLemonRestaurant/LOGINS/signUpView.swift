//
//  signUpView.swift
//  LittleLemonRestaurant
//
//  Created by Mwine on 14/03/2023.
//

import SwiftUI

struct signUpView: View {
    
    @State private var firstName = ""
    @State private var lastName = ""
    @State private var email = ""
    @State private var phoneNumber = ""
    var body: some View {
        ScrollView{
            LittleLemonLogo()
            Text("Personal information")
                .font(.headline)
                .frame(width:350, alignment: .leading)
            HStack(alignment: .center, spacing: 30){
                VStack(spacing:0){
                    
               
//                    Text("Avatar")
                    Image("woman_profile")
                        .resizable()
                        .aspectRatio(contentMode: .fit)
                        .clipShape(Circle())
                        .frame(width: 100)
                    
                }
                Text("Change")
                    .foregroundColor(.white)
                    .frame(width: 100, height: 50)
                    .background(Color("myGreen"))
                    .cornerRadius(5)
                
                Text("Remove")
                    .foregroundColor(Color("myGreen"))
                    .frame(width: 100, height: 50)
                    .border(Color("myGreen"))
                
            }.frame(width: 380)
            
            LazyVStack(spacing: 25){
                VStack(spacing: 10){
                    Text("First name")
                        .frame(width:350, alignment: .leading)
                    
                  
                        TextField("Enter your first name", text: $firstName)
                        .frame(width: 300)
                        .padding( )
                        .overlay(RoundedRectangle(cornerRadius: 10)
                            .stroke(Color("myGreen"), lineWidth: 1))}
                VStack(spacing: 10){
                    Text("Last name")
                        .frame(width:350, alignment: .leading)
                    
                  
                        TextField("Enter your last name", text: $lastName)
                        .frame(width: 300)
                        .padding( )
                        .overlay(RoundedRectangle(cornerRadius: 10)
                            .stroke(Color("myGreen"), lineWidth: 1))
                }
                VStack(spacing: 10){
                    Text("Email")
                        .frame(width:350, alignment: .leading)
                    
                  
                        TextField("Enter your email", text: $email)
                        .frame(width: 300)
                        .padding( )
                        .overlay(RoundedRectangle(cornerRadius: 10)
                            .stroke(Color("myGreen"), lineWidth: 1))
                }
                VStack(spacing: 10){
                    Text("Phone Number")
                        .frame(width:350, alignment: .leading)
                    
                  
                        TextField("Enter your phonenumber", text: $phoneNumber)
                        .frame(width: 300)
                        .padding( )
                        .overlay(RoundedRectangle(cornerRadius: 10)
                            .stroke(Color("myGreen"), lineWidth: 1))
                }
                
            }
            
            Text("Email notifications")
                .bold()
                .frame(width: 350, alignment: .leading)
                .padding(10)
            VStack(alignment: .leading){
                HStack{
                    Image(systemName: "checkmark.square.fill")
                        .foregroundColor(Color("myGreen"))
                    Text("Order Statuses")
                }
                HStack{
                    Image(systemName: "checkmark.square.fill")
                        .foregroundColor(Color("myGreen"))
                    Text("Password changes")
                }
                HStack{
                    Image(systemName: "checkmark.square.fill")
                        .foregroundColor(Color("myGreen"))
                    Text("Special offers")
                }
                HStack{
                    Image(systemName: "checkmark.square.fill")
                        .foregroundColor(Color("myGreen"))
                    Text("NewsLetters")
                }
            }.frame(width: 350 , alignment: .leading)
            
            ZStack{
                Rectangle()
                    .frame(width: 350, height: 50)
                    .foregroundColor(Color("myYellow"))
                    .cornerRadius(10)
                .padding(10)
                
                Text("Log out")
                    .bold()
                
            }
            
            HStack(spacing: 10){
                Text("Discard Changes")
                    .foregroundColor(Color("myGreen"))
                    .frame(width: 150, height: 50)
                    .border(Color("myGreen"))
                
                Text("Save changes")
                    .foregroundColor(.white)
                    .frame(width: 150, height: 50)
                    .background(Color("myGreen"))
                    .cornerRadius(5)
                
            
                
            }
                
            
        }}
}

struct signUpView_Previews: PreviewProvider {
    static var previews: some View {
        signUpView()
    }
}





  
